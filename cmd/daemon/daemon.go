package main

import (
	"gitlab.com/stnby/dumbchain/internal/daemon/discovery"
	"gitlab.com/stnby/dumbchain/internal/daemon/routes"
	"gitlab.com/stnby/dumbchain/internal/daemon/config"
)

func main() {
	// Parse cli arguments and fill in the config struct.
	var c config.Config
	config.Init(&c);

	// Start the DHT node.
	go discovery.Run()

	// Start the router.
	routes.Run(c)
}
