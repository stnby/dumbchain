package main

import (
	"gitlab.com/stnby/dumbchain/internal/namedaemon/discovery"
	"gitlab.com/stnby/dumbchain/internal/namedaemon/routes"
	"gitlab.com/stnby/dumbchain/internal/namedaemon/config"
)

func main() {
	// Parse cli arguments and fill in the config struct.
	config.Init();

	// Start the DHT node.
	go discovery.Run()

	// Start the router.
	routes.Run()
}
