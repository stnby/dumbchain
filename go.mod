module gitlab.com/stnby/dumbchain

go 1.16

require (
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/imroc/req v0.3.0 // indirect
	github.com/nictuku/dht v0.0.0-20201226073453-fd1c1dd3d66a // indirect
)
