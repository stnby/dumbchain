package config

import (
	"flag"
	"time"
)

const (
	DefaultPort = 7344

	// echo 'dumbchain' | sha1sum -
        InfoHash = "a9f5ad541c1afbbe19a617a9ca28b3826b66db5e"
)

type Config struct {
	BindPort uint
	AnnounceDHT bool
	TTL time.Duration
}

var Data Config

func Init() {
	flag.UintVar(&Data.BindPort, "port", DefaultPort, "Bind port for daemon api")
	flag.BoolVar(&Data.AnnounceDHT, "announce", true, "Announce ourselves")
	flag.DurationVar(&Data.TTL, "ttl", 2 * time.Minute, "Time to live")
	flag.Parse()
}
