package blockchain

import (
	"log"
	"time"
	"sync"
	"encoding/hex"
	"encoding/json"
	"crypto/sha512"
)

type Block struct {
	Height uint64 `json:"height"`
	Timestamp time.Time `json:"timestamp,string"`
	Hash *string `json:"hash"`
	Data *string `json:"data"`
}

type Chain struct {
	sync.Mutex
	Blocks map[string]Block `json:"blocks"`
	Latest *string `json:"latest"`
}

func CalculateHash(block *Block) string {
	j, err := json.Marshal(block)
	if err != nil {
		log.Fatal(err)
	}
	h := sha512.New384()
	h.Write(j)
	s := hex.EncodeToString(h.Sum(nil))
	log.Println("hash:", s)
	return s
}

func generateGenesisBlock() (*Block, string) {
	d := "genesis block"
	b := Block{
		Height: 0,
		Timestamp: time.Now(),
		// No hash. (null)
		Data: &d,
	}
	h := CalculateHash(&b)
	return &b, h
}

func (c *Chain) newBlock(prevHash *string, data *string) (*Block, *string) {
	b := Block{
		Height: c.Blocks[*prevHash].Height + 1,
		Timestamp: time.Now(),
		Hash: prevHash,
		Data: data,
	}
	h := CalculateHash(&b)
	return &b, &h
}

func (c *Chain) appendBlock(block *Block, hash *string) {
	c.Blocks[*hash] = *block
	c.Latest = hash
}

func (c *Chain) AppendNewBlock(data *string) (*Block, *string) {
	c.Lock()
	defer c.Unlock()
	b, h := c.newBlock(c.Latest, data)
	c.appendBlock(b, h)
	return b, h
}

func NewChain() *Chain {
	var c Chain
	c.Blocks = make(map[string]Block)
	b, h := generateGenesisBlock()
	c.appendBlock(b, &h)
	return &c
}
