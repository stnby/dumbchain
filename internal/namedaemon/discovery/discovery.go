package discovery

import (
	"log"
	"net"
	"time"
	"strconv"

	"github.com/nictuku/dht"
	"github.com/imroc/req"

	"gitlab.com/stnby/dumbchain/internal/namedaemon/config"
)

var (
	// This maps "IP:PORT" to HeartBeat.
	Nodes = map[string]time.Duration{}
	TTL = config.Data.TTL
)


func checkHeartBeat(ttl *time.Duration) {
	var t time.Duration
	var t0, t1 time.Time

	t0 = time.Now()
	for {
		t1, t, t0 = time.Now(), t1.Sub(t0), t1 // Gets time since last heartBeat
		for addr, heartbeat := range Nodes {
			if heartbeat-t < 1 {
				delete(Nodes, addr)
			} else {
				Nodes[addr] = heartbeat - t
			}
		}
	time.Sleep(*ttl / 5)
	}
}

func registerOurselves(ttl *time.Duration) {
	for {
		for host, _ := range Nodes {
			req.SetTimeout(1 * time.Second)
			_, err := req.Post("http://" + host + "/v1/discovery/" + strconv.FormatUint(uint64(config.Data.BindPort), 10))
			if err != nil {
				log.Println(err)
			}
		}
		time.Sleep(*ttl)
	}
}

func fetchNodes(host string) error {
	req.SetTimeout(1 * time.Second)
	res, err := req.Get("http://" + host + "/v1/discovery/")
	if err != nil {
		log.Println(err)
		return nil
	}
	type Response struct {
		TTL map[string]time.Duration `json:"ttl"`
	}
	var r Response
	err = res.ToJSON(&r)
	if err != nil {
		log.Println(err)
		return nil
	}
	for host, _ := range r.TTL {
		log.Println("peer (api):" + host)
		Nodes[host] = TTL
	}
	return nil
}

func drainNodes(n *dht.DHT) {
	for r := range n.PeersRequestResults {
		for _, peers := range r {
			for _, x := range peers {
				host := dht.DecodePeerAddress(x)
				ip, _, _ := net.SplitHostPort(host)
				host = ip + ":" + strconv.Itoa(config.DefaultPort)
				log.Println("peer (dht):", host)
				Nodes[host] = TTL
				fetchNodes(host)
			}
		}
	}
}

func Run() {
	ih, err := dht.DecodeInfoHash(config.InfoHash)

	// Start a DHT node on random port.
	d, err := dht.New(nil)
	if err != nil {
		log.Fatal(err)
	}
	err = d.Start()
	if err != nil {
		log.Fatal(err)
	}

	// Drain discovered nodes in a seperate goroutine.
	go drainNodes(d)

	go registerOurselves(&TTL)

	go checkHeartBeat(&TTL)

	// Keep requesting for more pairs in an endless loop.
	for {
		d.PeersRequest(string(ih), config.Data.AnnounceDHT)
		time.Sleep(30 * time.Second)
	}
}
