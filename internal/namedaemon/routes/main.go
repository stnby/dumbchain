package routes

import (
	"fmt"
	"github.com/gin-gonic/gin"

	"gitlab.com/stnby/dumbchain/internal/namedaemon/config"
)

var (
	r = gin.Default()
)

func getRoutes() {
	v1 := r.Group("/v1")
	addStatusRoutes(v1)
	addDiscoveryRoutes(v1)
	addBlockChainRoutes(v1)
}

// Start the API server.
func Run() {
	getRoutes()
	r.Run(fmt.Sprintf(":%d", config.Data.BindPort))
}
