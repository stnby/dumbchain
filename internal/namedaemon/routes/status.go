package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func addStatusRoutes(rg *gin.RouterGroup) {
	g := rg.Group("/status")

	g.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, "dumbchain")
	})
}
