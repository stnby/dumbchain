package routes

import (
	"time"
	"errors"
	"strconv"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/stnby/dumbchain/internal/namedaemon/discovery"
)

func validateNode(portStr string) (uint16, error) {
	port, err := strconv.ParseUint(portStr, 10, 16)
	if err != nil || port < 1 || port > 65535 {
		return 0, errors.New("invalid port")
	}
	// TODO: Also try connecting to this node to see if it actually is accessible.
	return uint16(port), nil
}

func getNodeTTL(host string) (time.Duration, error) {
	ttl, ok := discovery.Nodes[host]
	if !ok {
		return 0, errors.New("not registered or expired")
	}
	return ttl, nil
}

func addDiscoveryRoutes(rg *gin.RouterGroup) {
	g := rg.Group("/discovery")

	// Return list.
	g.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"ttl": discovery.Nodes,
		})
	})

	// Register.
	g.POST("/:port", func(c *gin.Context) {
		ip := c.ClientIP()
		portStr := c.Param("port")
		host := ip + ":" + portStr
		_, err := validateNode(portStr)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		_, err = getNodeTTL(host)
		if err == nil {
			c.JSON(http.StatusForbidden, gin.H{
				"error": "already registered",
			})
			return
		}
		discovery.Nodes[host] = discovery.TTL
		c.JSON(http.StatusOK, gin.H{
			"ttl": discovery.TTL,
			"value": host,
		})
	})

	// Ping.	
	g.GET("/:port", func(c *gin.Context) {
		ip := c.ClientIP()
		portStr := c.Param("port")
		host := ip + ":" + portStr
		_, err := validateNode(portStr)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		_, err = getNodeTTL(host)
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{
				"error": err.Error(),
			})
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"ttl": discovery.TTL, // perhaps we moddified the TTL in runtime.
			"value": host,
		})
	})
}
