package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/stnby/dumbchain/internal/namedaemon/blockchain"
)

var (
	bc = blockchain.NewChain()
)

func addBlockChainRoutes(rg *gin.RouterGroup) {
	g := rg.Group("/blockchain")

	// Return Blockchain.
	g.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, bc)
	})


	// Append New Block
	g.POST("/", func(c *gin.Context) {
		data := c.PostForm("data")
		// TODO: Limit data size.
		b, _ := bc.AppendNewBlock(&data)
		c.JSON(http.StatusOK, b)
	})
}
